import { Injectable } from '@angular/core';
import { Movie } from './Movie';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  private searchMovieAPI = "https://api.themoviedb.org/3/search/movie"
   private posterApi = "https://image.tmdb.org/t/p/w500";
   private singleMovie = "https://api.themoviedb.org/3/movie/";
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  constructor(
    private http: HttpClient
  ) { }

  getPopularMovies(): Observable<Movie[]> {
    const url = 'https://api.themoviedb.org/3/movie/popular?'
    return this.http.get<Movie[]>(url).pipe(
      map(this.responseHandler.bind(this)),
      catchError(this.handleError<Movie[]>('getMovies', []))
    );
  }

  getMovie(id: number): Observable<Movie> {
    const url= `${this.singleMovie}${id}?`;

    return this.http.get<Movie>(url).pipe(
      map(movie=>{
        movie.poster = `${this.posterApi}${movie.poster_path}`;
        movie.companies = movie.production_companies.map(company=>company.name).join(', ');
        movie.genres = movie.genres.map(genre=>genre.name).join(', ')
        return movie;
      }),
    catchError(this.handleError<Movie>(`getMovie id=${id}`))
  );
  }
  
  

  /* GET movies whose title contains search term */
searchMovies(term: string, page: number = 1): Observable<Movie[]> {
  if (!term.trim()) {
    // if not search term, return empty movie array.
    return of([]);
  }
  const url = `${this.searchMovieAPI}?&query=${term}&page=${page}`
  return this.http.get<Movie[]>(url).pipe(
    map(this.responseHandler.bind(this)),
    catchError(this.handleError<Movie[]>('searchMovies', []))
  );
}

private responseHandler(response) {
  return response.results.map(el=>{
    el.poster = `${this.posterApi}${el.poster_path}`;
    return el;
  })
}

private handleError<T> (operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {

    console.error(error); 
    return of(result as T);
  };
}
  
}
