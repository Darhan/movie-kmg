import { Injectable } from '@angular/core'
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http'
import { Observable, of } from 'rxjs'
@Injectable()
export class ApikeyInterceptor implements HttpInterceptor {
  private apiKey = "98325a9d3ed3ec225e41ccc4d360c817"
  private appLang = 'ru-Ru' // in the future we can get it from the tranlsate
  intercept(request: HttpRequest<any>, next: HttpHandler)
    : Observable<HttpEvent<any>> {
        console.log('request', request)
    if (request.url.includes('api.themoviedb.org')) { // for db requests add apiKey and language
        const apikeyReq = request.clone({
            url: request.url + "&api_key=" + this.apiKey + "&language=" + this.appLang
        });
      return next.handle(apikeyReq)
    }
    
    
    return next.handle(request)
  }
}