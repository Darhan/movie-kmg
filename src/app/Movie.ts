
export class Movie {
    id: number;
    name: string;
    poster_path: string;
    poster: string;
    genre: string;
    genres: string[];
    companies: string;
    production_companies: string[];
    popularity: number;
    vote_count: number;
    title: string;
    backdrop_path: string;
    original_title: string;
    original_language: string;
    adult: boolean;
    genre_ids: number[];
    vote_average: number;
    release_date: string;
    overview: string;

  }