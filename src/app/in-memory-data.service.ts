import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Movie } from './Movie';
import { MOVIES } from './mock-movies';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const movies = MOVIES
    return {movies};
  }

  genId(movies: Movie[]): number {
    return movies.length > 0 ? Math.max(...movies.map(movie => movie.id)) + 1 : 11;
  }
}